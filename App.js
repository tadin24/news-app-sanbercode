import React from 'react';
import {SafeAreaView} from 'react-native';
import {Provider} from 'react-redux';
import {RootStack} from './src/navigator/RootStack';
import {store} from './src/app/store';
import {NavigationContainer} from '@react-navigation/native';
import {appFirebase} from './src/app/services/firebase';

appFirebase;

const App = () => {
  return (
    <Provider store={store}>
      <NavigationContainer>
        <RootStack />
      </NavigationContainer>
    </Provider>
  );
};

export default App;
