import FacebookLogo from "./facebook.png";
import GithubLogo from "./github.png";
import GitlabLogo from "./gitlab.png";
import InstagramLogo from "./instagram.png";
import Logo from "./logo.jpg";
import TwitterLogo from "./twitter.png";
import UserLogo from "./user.png";

export {
  FacebookLogo,
  GithubLogo,
  GitlabLogo,
  InstagramLogo,
  Logo,
  TwitterLogo,
  UserLogo,
};
