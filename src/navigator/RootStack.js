import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {useDispatch, useSelector} from 'react-redux';
import {authState, hideSplash} from '../features/auth/authSlice';
import LoginScreen from '../screens/LoginScreen';
import HomeScreen from '../screens/HomeScreen';
import {
  titleAboutScreen,
  titleDetailScreen,
  titleHomeScreen,
  titleLoginScreen,
  titleMainAppScreen,
  titleRegisterScreen,
  titleSplashScreen,
} from '../app/const';
import SplashScreen from '../screens/SplashScreen';
import RegisterScreen from '../screens/RegisterScreen';
import DetailScreen from '../screens/DetailScreen';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import AboutScreen from '../screens/AboutScreen';

const Stack = createNativeStackNavigator();
const Tab = createBottomTabNavigator();

export const RootStack = () => {
  const dispatch = useDispatch();
  const {isSignedIn, isSplash} = useSelector(authState);

  setTimeout(() => {
    dispatch(hideSplash());
  }, 3000);

  return (
    <Stack.Navigator>
      {isSplash ? (
        <Stack.Screen
          name={titleSplashScreen}
          component={SplashScreen}
          options={{headerShown: false}}
        />
      ) : isSignedIn ? (
        <>
          <Stack.Screen
            name={titleMainAppScreen}
            component={MainApp}
            options={{headerShown: false}}
          />
          <Stack.Screen
            options={{title: 'Detail'}}
            name={titleDetailScreen}
            component={DetailScreen}
          />
        </>
      ) : (
        <>
          <Stack.Screen
            options={{headerShown: false}}
            name={titleLoginScreen}
            component={LoginScreen}
          />
          <Stack.Screen
            options={{headerShown: false}}
            name={titleRegisterScreen}
            component={RegisterScreen}
          />
        </>
      )}
    </Stack.Navigator>
  );
};

const MainApp = () => {
  return (
    <Tab.Navigator
      screenOptions={{
        tabBarLabelPosition: 'beside-icon',
        tabBarLabelStyle: {
          fontWeight: '700',
          fontSize: 15,
        },
        tabBarIconStyle: {display: 'none'},
      }}>
      <Tab.Screen
        name={titleHomeScreen}
        component={HomeScreen}
        options={{headerShown: false, tabBarLabel: 'Home'}}
      />
      <Tab.Screen
        name={titleAboutScreen}
        component={AboutScreen}
        options={{headerShown: false, tabBarLabel: 'About'}}
      />
    </Tab.Navigator>
  );
};
