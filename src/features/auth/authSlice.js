import {createSlice} from '@reduxjs/toolkit';
// import * as SecureStore from "expo-secure-store";
import {namaStrgToken} from '../../app/const';

const initialState = {
  isSignedIn: false,
  user: {email: 'tes'},
  isSplash: true,
};

export const authSlice = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    login: (state, action) => {
      const {user} = action.payload;
      state.isSignedIn = true;
      state.user = user;
      // SecureStore.setItemAsync(namaStrgToken, token);
    },
    logout: state => {
      state.isSignedIn = false;
      state.user = {};
      // SecureStore.deleteItemAsync(namaStrgToken);
    },
    hideSplash: state => {
      state.isSplash = false;
    },
  },
});

export const {login, logout, hideSplash} = authSlice.actions;

export const authState = state => state[authSlice.name];
