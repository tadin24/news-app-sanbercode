import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React from 'react';
import {bgPrimary, sizeSm, stylesGlobal, textBgPrimary} from '../../app/const';

const NewsItem = ({title, onPress}) => {
  return (
    <View
      style={{
        marginBottom: sizeSm,
        minHeight: 50,
        borderColor: bgPrimary,
        borderRadius: 5,
        borderWidth: 3,
        padding: 5,
      }}>
      <Text
        style={{flex: 1, flexWrap: 'wrap', fontSize: 17, marginBottom: sizeSm}}>
        {title}
      </Text>
      <TouchableOpacity
        onPress={onPress}
        style={{...stylesGlobal.btnMd, backgroundColor: bgPrimary}}>
        <Text style={{color: textBgPrimary, textAlign: 'center'}}>Detail</Text>
      </TouchableOpacity>
    </View>
  );
};

export default NewsItem;

const styles = StyleSheet.create({});
