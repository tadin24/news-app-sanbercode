import {createSlice} from '@reduxjs/toolkit';

const initialState = {
  listNews: [],
};

export const newsSlice = createSlice({
  name: 'news',
  initialState,
  reducers: {
    setList: (state, action) => {
      const {list} = action.payload;
      state.listNews = list;
    },
  },
});

export const {setList} = newsSlice.actions;

export const newsState = state => state[newsSlice.name];
