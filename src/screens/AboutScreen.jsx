import {
  Image,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import React from 'react';
import {
  FacebookLogo,
  GithubLogo,
  GitlabLogo,
  InstagramLogo,
  TwitterLogo,
  UserLogo,
} from '../assets';
import {useDispatch, useSelector} from 'react-redux';
import {authState, logout} from '../features/auth/authSlice';
import {
  bgPrimary,
  sizeLg,
  sizeMd,
  stylesGlobal,
  textBgPrimary,
} from '../app/const';

const AboutScreen = () => {
  const {user} = useSelector(authState);
  console.log(user);
  const dispatch = useDispatch();
  const fungsiSignOut = () => {
    dispatch(logout());
  };
  return (
    <SafeAreaView style={{...styles.container, paddingTop: 15}}>
      <ScrollView style={styles.scrollView}>
        <View style={styles.container}>
          <Text style={styles.title}>Tentang Saya</Text>
          <View style={styles.subTitle}>
            <Image style={styles.imageLogo} source={UserLogo} />
          </View>
          <>
            <Text style={styles.name}>{user.email}</Text>
            <Text style={styles.jobs}>React Native Developer</Text>
          </>
          <View style={styles.portfolio}>
            <View style={styles.contentPortfolio}>
              <Text>Portfolio</Text>
              <View style={styles.contentSkill}>
                <View style={styles.subContentSkill}>
                  <Image style={styles.imgPortfolio} source={GitlabLogo} />
                  <Text>Gitlab</Text>
                </View>
                <View style={styles.subContentSkill}>
                  <Image style={styles.imgPortfolio} source={GithubLogo} />
                  <Text>Github</Text>
                </View>
              </View>
            </View>
          </View>
          <View style={styles.socialMedia}>
            <View style={styles.contentSocialMedia}>
              <Text>Hubungi Saya</Text>
              <View style={styles.contentSocialMediaDown}>
                <View style={styles.subContentSocialMediaDown}>
                  <>
                    <Image style={styles.imgPortfolio} source={FacebookLogo} />
                    <Text>Facebook</Text>
                  </>
                  <>
                    <Image style={styles.imgPortfolio} source={TwitterLogo} />
                    <Text>Twitter</Text>
                  </>
                  <>
                    <Image style={styles.imgPortfolio} source={InstagramLogo} />
                    <Text>Instagram</Text>
                  </>
                </View>
              </View>
            </View>
          </View>
          <TouchableOpacity
            onPress={fungsiSignOut}
            style={{
              ...stylesGlobal.btn,
              ...stylesGlobal.btnMd,
              ...stylesGlobal.btnBlock,
              backgroundColor: bgPrimary,
            }}>
            <Text
              style={{
                color: textBgPrimary,
                fontSize: sizeLg,
                textAlign: 'center',
              }}>
              Sign Out
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default AboutScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: 'white',
  },
  scrollView: {
    backgroundColor: 'pink',
    marginHorizontal: 20,
  },
  imageLogo: {
    height: 136,
    width: 136,
    resizeMode: 'stretch',
    marginBottom: 20,
    alignSelf: 'center',
    marginTop: 20,
  },
  title: {
    fontSize: 36,
    fontWeight: '700',
    marginBottom: 20,
  },
  subTitle: {
    backgroundColor: 'grey',
    height: 200,
    width: 200,
    borderRadius: 100,
  },
  name: {
    fontSize: 24,
    fontWeight: '700',
    color: 'blue',
  },
  jobs: {
    fontSize: 16,
    fontWeight: '400',
    color: 'blue',
  },
  portfolio: {
    backgroundColor: '#efefef',
    height: 140,
    width: 300,
  },
  contentPortfolio: {
    padding: 5,
  },
  contentSkill: {
    padding: 10,
    flexDirection: 'row',
    justifyContent: 'space-around',
    height: 90,
    marginTop: 10,
  },
  subContentSkill: {
    flexDirection: 'column',
    paddingBottom: 5,
  },
  socialMedia: {
    marginTop: 10,
    backgroundColor: '#efefef',
    height: 300,
    width: 300,
  },
  contentSocialMedia: {
    padding: 8,
  },
  contentSocialMediaDown: {
    flexDirection: 'column',
    justifyContent: 'space-around',
  },
  subContentSocialMediaDown: {
    alignSelf: 'center',
    marginTop: 10,
  },
  imgPortfolio: {
    height: 30,
    width: 30,
  },
});
