import {Alert, FlatList, StyleSheet, Text, View} from 'react-native';
import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import newAxios from '../app/services/client';
import {newsState, setList} from '../features/news/newsSlice';
import NewsItem from '../features/news/NewsItem';
import {sizeXl, stylesGlobal, titleDetailScreen} from '../app/const';

const HomeScreen = ({navigation}) => {
  const dispatch = useDispatch();
  const [isLoading, setIsLoading] = useState(false);
  const {listNews} = useSelector(newsState);

  const getNews = async () => {
    setIsLoading(true);
    await newAxios
      .get('/posts')
      .then(res => {
        setIsLoading(false);
        const newNews = [];
        for (let k = 0; k < 10; k++) {
          newNews.push(res.data[k]);
        }
        dispatch(setList({list: newNews}));
      })
      .catch(err => {
        setIsLoading(false);
        Alert.alert(err.message);
      });
  };

  const renderNews = ({item}) => {
    const onPress = () => navigation.navigate(titleDetailScreen, {item: item});

    return <NewsItem title={item.title} onPress={onPress} />;
  };

  useEffect(() => {
    getNews();
  }, []);

  return (
    <>
      <View
        style={{
          justifyContent: 'center',
          alignItems: 'center',
          height: 50,
        }}>
        <Text style={{fontSize: sizeXl}}>List Berita</Text>
      </View>
      {isLoading ? (
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
          <Text>Loading...</Text>
        </View>
      ) : (
        <View style={{...stylesGlobal.container, paddingVertical: 0}}>
          <FlatList
            data={listNews}
            keyExtractor={item => item.id}
            renderItem={renderNews}
          />
        </View>
      )}
    </>
  );
};

export default HomeScreen;

const styles = StyleSheet.create({});
