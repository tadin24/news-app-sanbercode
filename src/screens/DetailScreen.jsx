import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {
  bgPrimary,
  boldMd,
  sizeMd,
  sizeSm,
  sizeXl,
  stylesGlobal,
} from '../app/const';

const DetailScreen = ({route, navigation}) => {
  const {title, body} = route.params.item;
  return (
    <View
      style={{
        ...stylesGlobal.container,
        alignItems: 'flex-start',
        justifyContent: 'flex-start',
      }}>
      <View
        style={{
          borderColor: bgPrimary,
          borderWidth: 3,
          borderRadius: sizeSm,
          padding: 5,
        }}>
        <Text
          style={{
            fontSize: sizeXl,
            fontWeight: boldMd,
            textTransform: 'capitalize',
            textAlign: 'justify',
          }}>
          {title}
        </Text>
        <View style={{height: sizeSm}}></View>
        <Text style={{textAlign: 'justify', fontSize: sizeMd}}>{body}</Text>
      </View>
    </View>
  );
};

export default DetailScreen;

const styles = StyleSheet.create({});
