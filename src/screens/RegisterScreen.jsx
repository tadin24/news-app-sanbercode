import {
  Alert,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import React, {useState} from 'react';
import {
  bgPrimary,
  boldLg,
  sizeMd,
  sizeSm,
  sizeXxl,
  stylesGlobal,
  textBgPrimary,
  titleLoginScreen,
} from '../app/const';
import firebase from 'firebase/compat';

const RegisterScreen = ({navigation}) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const submitData = async () => {
    try {
      const rsp = await firebase
        .auth()
        .createUserWithEmailAndPassword(email, password);

      if (rsp.user) {
        Alert.alert('Berhasil mendaftar! Silahkan login');
        navigation.navigate(titleLoginScreen);
      }
    } catch (error) {
      Alert.alert(error.message);
    }
  };

  return (
    <View style={{...stylesGlobal.container}}>
      <Text style={{fontSize: sizeXxl, fontWeight: boldLg}}>Register</Text>
      <Text
        style={{
          ...stylesGlobal.inputLabel,
          fontSize: sizeMd,
        }}>
        Email
      </Text>
      <TextInput
        style={stylesGlobal.inputContainer}
        value={email}
        onChangeText={e => setEmail(e)}
        placeholder="Masukkan email"
      />
      <Text
        style={{
          ...stylesGlobal.inputLabel,
          fontSize: sizeMd,
        }}>
        Password
      </Text>
      <TextInput
        style={stylesGlobal.inputContainer}
        value={password}
        onChangeText={e => setPassword(e)}
        placeholder="Masukkan password"
      />
      <TouchableOpacity
        style={{
          ...stylesGlobal.btnMd,
          ...stylesGlobal.btnBlock,
          backgroundColor: bgPrimary,
        }}
        onPress={submitData}>
        <Text style={{...stylesGlobal.btnTextMd, color: textBgPrimary}}>
          Register
        </Text>
      </TouchableOpacity>
      <View style={{height: sizeSm}}></View>
      <Text style={{marginRight: 'auto', fontSize: sizeMd}}>
        Sudah memiliki akun? Silahkan{' '}
        <Text
          style={{color: bgPrimary}}
          onPress={() => navigation.navigate(titleLoginScreen)}>
          login
        </Text>
      </Text>
    </View>
  );
};

export default RegisterScreen;

const styles = StyleSheet.create({});
