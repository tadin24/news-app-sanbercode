import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {bgPrimary, sizeXxl, stylesGlobal, textBgPrimary} from '../app/const';

const SplashScreen = () => {
  return (
    <View style={{...stylesGlobal.container, backgroundColor: bgPrimary}}>
      <Text
        style={{color: textBgPrimary, fontSize: sizeXxl, fontStyle: 'italic'}}>
        12News
      </Text>
    </View>
  );
};

export default SplashScreen;

const styles = StyleSheet.create({});
