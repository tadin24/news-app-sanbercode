import {
  Alert,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import React, {useState} from 'react';
import {useDispatch} from 'react-redux';
import {
  bgPrimary,
  boldLg,
  sizeMd,
  sizeSm,
  sizeXxl,
  stylesGlobal,
  textBgPrimary,
  titleRegisterScreen,
} from '../app/const';
import firebase from 'firebase/compat';
import {login} from '../features/auth/authSlice';

const LoginScreen = ({navigation}) => {
  const dispatch = useDispatch();
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const submitData = async () => {
    try {
      const rsp = await firebase
        .auth()
        .signInWithEmailAndPassword(email, password);

      if (rsp.user) {
        Alert.alert('Berhasil login!');
        dispatch(login({user: rsp.user}));
      }
    } catch (error) {
      Alert.alert(error.message);
    }
  };

  return (
    <View style={{...stylesGlobal.container}}>
      <Text style={{fontSize: sizeXxl, fontWeight: boldLg}}>Login</Text>
      <Text
        style={{
          ...stylesGlobal.inputLabel,
          fontSize: sizeMd,
        }}>
        Email
      </Text>
      <TextInput
        style={stylesGlobal.inputContainer}
        value={email}
        onChangeText={e => setEmail(e)}
        placeholder="Masukkan email"
      />
      <Text
        style={{
          ...stylesGlobal.inputLabel,
          fontSize: sizeMd,
        }}>
        Password
      </Text>
      <TextInput
        style={stylesGlobal.inputContainer}
        value={password}
        onChangeText={e => setPassword(e)}
        placeholder="Masukkan password"
      />
      <TouchableOpacity
        style={{
          ...stylesGlobal.btnMd,
          ...stylesGlobal.btnBlock,
          backgroundColor: bgPrimary,
        }}
        onPress={submitData}>
        <Text style={{...stylesGlobal.btnTextMd, color: textBgPrimary}}>
          Login
        </Text>
      </TouchableOpacity>
      <View style={{height: sizeSm}}></View>
      <Text style={{marginRight: 'auto', fontSize: sizeMd}}>
        Belum memiliki akun? Buat{' '}
        <Text
          style={{color: bgPrimary}}
          onPress={() => navigation.navigate(titleRegisterScreen)}>
          Akun
        </Text>{' '}
        terlebih dahulu
      </Text>
    </View>
  );
};

export default LoginScreen;

const styles = StyleSheet.create({});
