import {configureStore} from '@reduxjs/toolkit';
import {authSlice} from '../features/auth/authSlice';
import {newsSlice} from '../features/news/newsSlice';

export const store = configureStore({
  reducer: {
    [authSlice.name]: authSlice.reducer,
    [newsSlice.name]: newsSlice.reducer,
  },
});
