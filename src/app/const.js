import {StyleSheet} from 'react-native';

export const titleAboutScreen = 'AboutScreen';
export const titleDetailScreen = 'DetailScreen';
export const titleHomeScreen = 'HomeScreen';
export const titleLoginScreen = 'LoginScreen';
export const titleMainAppScreen = 'MainAppScreen';
export const titleRegisterScreen = 'RegisterScreen';
export const titleSplashScreen = 'SplashScreen';

// nama local storage penyimpanan
export const namaStrgToken = 'token';

// size
export const sizeXxl = 35;
export const sizeXl = 22;
export const sizeLg = 18;
export const sizeMd = 14;
export const sizeSm = 10;

// bold
export const boldXl = '900';
export const boldLg = '700';
export const boldMd = '500';
export const boldSm = '300';

// color
export const bgPrimary = '#023047';
export const textBgPrimary = '#eee';

export const stylesGlobal = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: '10%',
    paddingVertical: '10%',
  },
  inputContainer: {
    borderColor: '#343434',
    borderRadius: 3,
    borderWidth: 1,
    width: '100%',
    paddingHorizontal: 10,
    paddingVertical: 10,
    marginBottom: 10,
  },
  inputLabel: {
    marginBottom: 10,
    textAlign: 'left',
  },
  btnMd: {
    paddingHorizontal: 20,
    paddingVertical: 10,
  },
  btnTextMd: {
    fontSize: 14,
    textAlign: 'center',
  },
  btnBlock: {
    width: '100%',
  },
});
