import firebase from 'firebase/compat/app';
// import firebase from 'firebase';

export const firebaseConfig = {
  apiKey: 'AIzaSyBGWpBECpUPXiQOTsx3ulS7ftt_XViU8cw',
  authDomain: 'sanber-ecommerce.firebaseapp.com',
  projectId: 'sanber-ecommerce',
  storageBucket: 'sanber-ecommerce.appspot.com',
  messagingSenderId: '939019798078',
  appId: '1:939019798078:web:e2c04dd7a1782aefe9736f',
  measurementId: 'G-7HGBYFD3B7',
};

if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
}

export const appFirebase = firebase;
